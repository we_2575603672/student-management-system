package com.chs.students.controller;


import com.baomidou.mybatisplus.plugins.Page;
import com.chs.students.conf.Result;
import com.chs.students.conf.StatusCode;
import com.chs.students.entity.Lass;
import com.chs.students.service.LassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2020-12-02
 */
@RestController
@CrossOrigin
@RequestMapping("/class")
public class LassController {
    @Autowired
    private LassService lassService;
    @GetMapping("/getClassByPage/{page}/{size}")
    public Result<Page<Lass>> getAllQuestionByPage(@PathVariable Integer page, @PathVariable Integer size) {
        Page<Lass> questionPage = lassService.selectPage(new Page<>(page, size));
        return new Result<Page<Lass>>(true, StatusCode.OK,"查询成功",questionPage);
    }
    @GetMapping(value="search/{id}")
    public Result<Lass> search(@PathVariable String id){
        Lass lass = new Lass();
        lass.setClassName(id);
        Lass lasses = lassService.selectOne(lass);
        return new Result<Lass>(true,StatusCode.OK,"查询成功",lasses);
    }
    //删除
    @DeleteMapping(value = "/{id}" )
    public Result delete(@PathVariable Integer id){
        //调用StudentsService实现根据主键删除
        lassService.deleteById(id);
        return new Result(true, StatusCode.OK,"删除成功");
    }
    //修改
    @PutMapping(value="/{id}")
    public Result update(@RequestBody Lass lass, @PathVariable int id){
        //设置主键值
        lass.setId(id);
        //调用StudentsService实现修改Students
        lassService.updateById(lass);
        return new Result(true,StatusCode.OK,"修改成功");
    }

    //添加
    @PostMapping
    public Result add(@RequestBody   Lass lass){
        //调用StudentsService实现添加Students
        lassService.insert(lass);
        return new Result(true,StatusCode.OK,"添加成功");
    }
}

