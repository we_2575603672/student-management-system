package com.chs.students.controller;


import com.baomidou.mybatisplus.plugins.Page;
import com.chs.students.conf.Result;
import com.chs.students.conf.StatusCode;
import com.chs.students.entity.Lass;
import com.chs.students.entity.Students;
import com.chs.students.service.LassService;
import com.chs.students.service.StudentsService;
import com.chs.students.entity.StudentsClassVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2020-12-02
 */
@RestController
@CrossOrigin
@RequestMapping("/students")
public class StudentsController {
    @Autowired
    private StudentsService studentsService;
    @Autowired
    private LassService lassService;
    @GetMapping("/getStudentsClassByPage/{page}/{size}")
    public Result<Page<StudentsClassVO>> getAllQuestionByPage(@PathVariable Integer page, @PathVariable Integer size) {
        Page<StudentsClassVO> questionPage = studentsService.getStudentClass(new Page<>(page, size));
        return new Result<Page<StudentsClassVO>>(true, StatusCode.OK,"查询成功",questionPage);
    }
    //删除
    @DeleteMapping(value = "/{id}" )
    public Result delete(@PathVariable String id){
        studentsService.deleteById(id);
        return new Result(true, StatusCode.OK,"删除成功");
    }
    //修改
    @PutMapping(value="/{id}")
    public Result update(@RequestBody Students students,@PathVariable String id){
        //设置主键值
        students.setStudentId(id);
        //调用StudentsService实现修改Students
        studentsService.updateById(students);
        return new Result(true,StatusCode.OK,"修改成功");
    }

    @GetMapping(value="search/{id}")
    public Result<StudentsClassVO> search(@PathVariable String id){

        StudentsClassVO students = studentsService.getStudentsById(id);
        return new Result<StudentsClassVO>(true,StatusCode.OK,"查询成功",students);
    }
    //添加
    @PostMapping
    public Result add(@RequestBody   Students students){
        //调用StudentsService实现添加Students
        studentsService.insert(students);
        return new Result(true,StatusCode.OK,"添加成功");
    }
    //查询班级
    @GetMapping("/getClass")
    public Result<List<Lass>> getClasss(){
        //调用StudentsService实现添加Students
        List<Lass> lasses = lassService.selectList(null);
        return new Result<List<Lass>>(true,StatusCode.OK,"查询成功",lasses);
    }

}

