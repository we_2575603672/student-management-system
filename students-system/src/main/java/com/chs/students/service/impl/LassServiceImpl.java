package com.chs.students.service.impl;

import com.chs.students.entity.Lass;
import com.chs.students.mapper.LassMapper;
import com.chs.students.service.LassService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-02
 */
@Service
public class LassServiceImpl extends ServiceImpl<LassMapper, Lass> implements LassService {

    @Autowired
    private LassMapper lassMapper;

    @Override
    public Lass selectOne(Lass lass) {
        return lassMapper.selectOne(lass);
    }
}
