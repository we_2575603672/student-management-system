package com.chs.students.service;

import com.chs.students.entity.Lass;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-02
 */
public interface LassService extends IService<Lass> {

    Lass selectOne(Lass lass);
}
