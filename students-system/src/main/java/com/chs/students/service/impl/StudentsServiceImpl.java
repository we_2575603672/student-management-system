package com.chs.students.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.chs.students.entity.Students;
import com.chs.students.mapper.StudentsMapper;
import com.chs.students.service.StudentsService;
import com.chs.students.entity.StudentsClassVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-02
 */
@Service
public class StudentsServiceImpl extends ServiceImpl<StudentsMapper, Students> implements StudentsService {
    @Autowired
    private StudentsMapper studentsMapper;

    @Override
    public Page<StudentsClassVO> getStudentClass(Page<StudentsClassVO> page) {
        return page.setRecords(this.baseMapper.getStudentsClass(page));
    }

    @Override
    public StudentsClassVO getStudentsById(String id) {
        return studentsMapper.getStudentsById(id);
    }


    @Override
    public boolean deleteById(Serializable id) {
        return super.deleteById(id);
    }
}
