package com.chs.students.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.chs.students.entity.Students;
import com.baomidou.mybatisplus.service.IService;
import com.chs.students.entity.StudentsClassVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-02
 */
public interface StudentsService extends IService<Students> {
    Page<StudentsClassVO> getStudentClass(Page<StudentsClassVO> page);
    StudentsClassVO getStudentsById(String id);
}
