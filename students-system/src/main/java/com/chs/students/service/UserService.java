package com.chs.students.service;



import com.chs.students.entity.User;
import com.chs.students.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/****
 * @Author:Chs
 * @Description:User业务层实现类
 * @Date 2019/6/14 0:16
 *****/
@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserMapper userMapper;

    /****
     * 通过用户名去数据库中查找用户
     * 如果找到就继续查找该用户具有的角色信息，找不到抛出异常
     * @param username
     * @return 角色信息
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userMapper.loadUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("账户不存在!");
        }
        user.setRoles(userMapper.getUserRolesByUid(user.getId()));
        return user;
    }
}
