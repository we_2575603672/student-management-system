package com.chs.students.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * @author Chs
 * @Classname StudentsClassVO
 * @Description TODO
 * @Date 2020/12/2 21:33
 * @Version V1.0
 */

public class StudentsClassVO implements Serializable {
    /**
     * 学号
     */
    @TableId(value = "student_id", type = IdType.AUTO)
    private String studentId;

    /**
     * 姓名
     */
    @TableField("student_name")
    private String studentName;

    /**
     * 性别
     */
    @TableField("student_gender")
    private String studentGender;

    /**
     * 出生年月
     */
    private Integer age;

    /**
     * 手机号码
     */
    private String cellphone;

    /**
     * 班级
     */
    @TableField("class_id")
    private Integer classId;
    /**
     * 班级名称
     */
    @TableField("class_name")
    private String className;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentGender() {
        return studentGender;
    }

    public void setStudentGender(String studentGender) {
        this.studentGender = studentGender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
