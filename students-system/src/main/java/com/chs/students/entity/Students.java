package com.chs.students.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-12-02
 */
@TableName("t_students")
public class Students implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 学号
     */
    @TableId(value = "student_id")
    private String studentId;

    /**
     * 姓名
     */
    private String studentName;

    /**
     * 性别
     */
    private String studentGender;

    /**
     * 出生年月
     */
    private Integer age;

    /**
     * 手机号码
     */
    private String cellphone;

    /**
     * 班级
     */
    private Integer classId;


    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentGender() {
        return studentGender;
    }

    public void setStudentGender(String studentGender) {
        this.studentGender = studentGender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    @Override
    public String toString() {
        return "Students{" +
        "studentId=" + studentId +
        ", studentName=" + studentName +
        ", studentGender=" + studentGender +
        ", age=" + age +
        ", cellphone=" + cellphone +
        ", classId=" + classId +
        "}";
    }
}
