package com.chs.students.entity;

/****
 * @Author:Chs
 * @Description:Role构建
 * @Date 2020/11/28 10:35
 *****/
public class Role {
    private Integer id;
    private String name;
    private String nameZh;
    //省略getter/setter

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameZh() {
        return nameZh;
    }

    public void setNameZh(String nameZh) {
        this.nameZh = nameZh;
    }
}
