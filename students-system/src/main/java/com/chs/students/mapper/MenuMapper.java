package com.chs.students.mapper;


import com.chs.students.entity.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/****
 * @Author:Chs
 * @Description:Menu的mapper
 * @Date 2020/11/28 10:35
 *****/
@Mapper
public interface MenuMapper {
    List<Menu> getAllMenus();
}
