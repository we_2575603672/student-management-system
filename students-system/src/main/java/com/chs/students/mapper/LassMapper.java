package com.chs.students.mapper;

import com.chs.students.entity.Lass;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-12-02
 */
public interface LassMapper extends BaseMapper<Lass> {



}
