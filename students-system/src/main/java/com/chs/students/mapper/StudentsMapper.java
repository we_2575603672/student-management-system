package com.chs.students.mapper;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.chs.students.entity.Students;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.chs.students.entity.StudentsClassVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-12-02
 */
public interface StudentsMapper extends BaseMapper<Students> {
    @Select("SELECT t_students.*,t_class.`class_name` FROM t_class,t_students WHERE t_students.class_id=t_class.id")
    List<StudentsClassVO> getStudentsClass(Pagination page);

    @Select("SELECT t_students.*,t_class.`class_name` FROM t_class,t_students WHERE t_students.class_id=t_class.id and t_students.student_id=#{id}")
    StudentsClassVO getStudentsById(@Param("id") String id);
}
