package com.chs.students.mapper;


import com.chs.students.entity.Role;
import com.chs.students.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/****
 * @Author:Chs
 * @Description:User的mapper
 * @Date 2020/11/28 10:35
 *****/
@Mapper
public interface UserMapper {
    User loadUserByUsername(String username);
    List<Role> getUserRolesByUid(Integer id);
}
