package com.chs.students;


import com.chs.students.entity.Students;
import com.chs.students.mapper.LassMapper;
import com.chs.students.mapper.StudentsMapper;
import com.chs.students.service.StudentsService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest

@MapperScan(basePackages = "com.chs.students.mapper")
class StudentsApplicationTests {


    @Autowired
    StudentsService studentsService;

    @Autowired
    StudentsMapper studentsMapper;
    @Autowired
    LassMapper lassMapper;

    @Test
    void contextLoads() {
        Students students = new Students();
        students = studentsMapper.selectById("2000");

        for (int i = 1; i < 50; i++) {

            students.setStudentId( String.valueOf(Integer.valueOf(students.getStudentId())+i));
            students.setStudentName("张山"+i);
            studentsService.insert(students);
        }
//        Lass lass = new Lass();
//        lass.setClassName("软件技术301");
//       System.out.println(lassMapper.selectOne(lass));


//            Map<String, Object> map = new HashMap<>();
//            Page<Question> questionPage = questionService.selectPage(new Page<>(1, 3));
//            if (questionPage.getRecords().size() == 0) {
//                map.put("code", 400);
//            } else {
//                map.put("code", 200);
//                map.put("data", questionPage);
//            }
//        System.out.println(map);
    }

}
