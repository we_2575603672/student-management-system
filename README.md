# student-management-system

#### 介绍
学生管理系统
前后端分离技术
框架：springboot+mybatis-puls+vue-cli+element-ui
实现了用户登录、学生信息的增删改查、班级信息的增删改查，权限操作。


#### 软件架构
软件架构说明
后端：springboot+mybatis-puls
前端：vuecli+element-ui

#### 安装教程

1.  用idea部署后端
2.  用VScode部署前端
3.下载所需的jar包和modules
#### 使用说明

1.  前后端都运行后使用前端端口即可使用：http://127.0.0.1:8081/
2.  前端端口：8081
3.  后端端口：8080

![登录界面](https://images.gitee.com/uploads/images/2020/1214/205446_9513aa1f_7673156.png "屏幕截图.png")
![首页](https://images.gitee.com/uploads/images/2020/1214/205509_c5c01abf_7673156.png "屏幕截图.png")
![添加页面](https://images.gitee.com/uploads/images/2020/1214/205535_448c9196_7673156.png "屏幕截图.png")
![修改页面](https://images.gitee.com/uploads/images/2020/1214/205556_c707af8c_7673156.png "屏幕截图.png")
![班级页面](https://images.gitee.com/uploads/images/2020/1214/205619_dc87ae51_7673156.png "屏幕截图.png")