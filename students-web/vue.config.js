module.exports = {
    outputDir: 'dist',   //build输出目录
    assetsDir: 'assets', 
    lintOnSave: false, 
    devServer: {
        open: true, 
        host: "127.0.0.1",
        port: '8081',
        proxy: {
            '/api': {
                target: 'http://127.0.0.1:8080',
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    '^/api': '/'
                }
            }

        },
    }
}