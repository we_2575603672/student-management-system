/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:3306
 Source Schema         : students

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 14/12/2020 20:45:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_class
-- ----------------------------
DROP TABLE IF EXISTS `t_class`;
CREATE TABLE `t_class`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '班级id',
  `class_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '班级名称',
  `class_teacher` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '班主任',
  PRIMARY KEY (`id`, `class_name`) USING BTREE,
  INDEX `class_id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 113 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_class
-- ----------------------------
INSERT INTO `t_class` VALUES (101, '18软件技术301', 'ww');
INSERT INTO `t_class` VALUES (102, '18软件技术302', NULL);
INSERT INTO `t_class` VALUES (103, '18软件技术303', NULL);
INSERT INTO `t_class` VALUES (104, '18软件技术304', NULL);
INSERT INTO `t_class` VALUES (105, '18软件技术305', NULL);
INSERT INTO `t_class` VALUES (107, '18软件技术307', NULL);
INSERT INTO `t_class` VALUES (108, '18软件技术308', NULL);
INSERT INTO `t_class` VALUES (109, '18软件技术309', NULL);
INSERT INTO `t_class` VALUES (110, '18软件技术310', NULL);
INSERT INTO `t_class` VALUES (111, '18软件技术311', NULL);
INSERT INTO `t_class` VALUES (112, '18软件技术312', NULL);

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu`  (
  `id` int(11) NOT NULL COMMENT 'ID',
  `pattern` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '路径',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES (1, '/class/**');
INSERT INTO `t_menu` VALUES (2, '/students/**');

-- ----------------------------
-- Table structure for t_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `t_menu_role`;
CREATE TABLE `t_menu_role`  (
  `id` int(11) NOT NULL COMMENT 'ID',
  `mid` int(11) NULL DEFAULT NULL COMMENT 'MENU',
  `rid` int(11) NULL DEFAULT NULL COMMENT 'ROLE',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_menu_role
-- ----------------------------
INSERT INTO `t_menu_role` VALUES (1, 1, 1);
INSERT INTO `t_menu_role` VALUES (2, 2, 2);

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nameZh` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES (1, 'admin', '系统管理员');
INSERT INTO `t_role` VALUES (2, 'user', '用户');

-- ----------------------------
-- Table structure for t_students
-- ----------------------------
DROP TABLE IF EXISTS `t_students`;
CREATE TABLE `t_students`  (
  `student_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学号',
  `student_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `student_gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `age` int(3) NULL DEFAULT NULL COMMENT '出生年月',
  `cellphone` varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `class_id` int(11) NULL DEFAULT NULL COMMENT '班级',
  PRIMARY KEY (`student_id`) USING BTREE,
  INDEX `index_class`(`class_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_students
-- ----------------------------
INSERT INTO `t_students` VALUES ('2000', '张三', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2001', '张山1', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2003', '张山2', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2006', '张山3', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2010', '张山4', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2015', '张山5', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2021', '张山6', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2028', '张山7', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2036', '张山8', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2045', '张山9', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2055', '张山10', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2066', '张山11', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2078', '张山12', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2091', '张山13', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2105', '张山14', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2120', '张山15', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2136', '张山16', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2153', '张山17', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2171', '张山18', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2190', '张山19', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2210', '张山20', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2231', '张山21', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2253', '张山22', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2276', '张山23', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2300', '张山24', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2325', '张山25', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2351', '张山26', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2378', '张山27', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2406', '张山28', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2435', '张山29', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2465', '张山30', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2496', '张山31', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2528', '张山32', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2561', '张山33', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2595', '张山34', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2630', '张山35', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2666', '张山36', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2703', '张山37', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2741', '张山38', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2780', '张山39', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2820', '张山40', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2861', '张山41', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2903', '张山42', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2946', '张山43', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('2990', '张山44', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('3035', '张山45', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('3081', '张山46', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('3128', '张山47', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('3176', '张山48', '男', 22, '13755565252', 101);
INSERT INTO `t_students` VALUES ('3225', '张山49', '男', 22, '13755565252', 101);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `enabled` tinyint(1) NULL DEFAULT 1,
  `locked` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, 'root', '$2a$10$RMuFXGQ5AtH4wOvkUqyvuecpqUSeoxZYqilXzbz50dceRsga.WYiq', 1, 0);
INSERT INTO `t_user` VALUES (2, 'admin', '$2a$10$RMuFXGQ5AtH4wOvkUqyvuecpqUSeoxZYqilXzbz50dceRsga.WYiq', 1, 0);
INSERT INTO `t_user` VALUES (3, 'zhanglongfei', '$2a$10$RMuFXGQ5AtH4wOvkUqyvuecpqUSeoxZYqilXzbz50dceRsga.WYiq', 1, 0);

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT NULL,
  `rid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES (1, 1, 1);
INSERT INTO `t_user_role` VALUES (2, 1, 2);
INSERT INTO `t_user_role` VALUES (3, 2, 2);
INSERT INTO `t_user_role` VALUES (4, 3, 2);

SET FOREIGN_KEY_CHECKS = 1;
